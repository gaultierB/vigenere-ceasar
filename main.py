import math
def clean(text):
    accent_correspondance = {
        "e": ["è", "é", "ê", "ë"],
        "a": ["à", "á", "â", "ã", "ä", "å", "æ"],
        "u": ["ù", "ú", "û", "ü"],
        "o": ["ð", "ò", "ó", "ô", "õ", "ö", "ø", "œ"],
        "i": ["ì", "í", "î", "ï"],
        "y": ["ý", "ÿ"],
        "c": ["ç"]
    }
    lower_text = text.lower()
    result = lower_text
    i = 0
    for char in result:
        for letter, accent in accent_correspondance.items():
            if char in accent:
                result = result.replace(char, letter)
    return result


def occurenceOfLetter(text, alphalist):
    alphalistx = list(alphalist)
    i = 0
    for char in alphalistx:
        alphalistx[i] = text.count(char)
        i += 1
    return alphalistx


def rateOfLetter(text, alphalist):
    alphalisty = list(alphalist)
    len_text = len(text)
    if len_text == 0:
        print("error")
        return
    i = 0
    for occurenceOfLetter in alphalisty:
        alphalisty[i] = ('%.3f' % (occurenceOfLetter / len_text)).rstrip('0').rstrip('.')
        i += 1
    return list(alphalisty)


def openFile(filename):
    file = open(filename, "r", encoding='utf-8')
    resulttext = ""
    for x in file:
        resulttext = resulttext + x
    return str(resulttext)


def writeFile(filename, text):
    with open(filename, "w", encoding='utf-8') as file:
        file.write(text)


def charToCesar(char, key, alphalist):
    if char in alphalist:
        ialphabet = (alphalist.index(char) + key) % 26
        return alphalist[ialphabet]
    else:
        return char


def cesarToChar(char, key, alphalist):
    if char in alphalist:
        ialphabet = (alphalist.index(char) - key) % 26
        return alphalist[ialphabet]
    else:
        return char


def textToCesar(text, key, alphalist):
    textResult = ""
    for char in text:
        textResult = textResult + charToCesar(char, key, alphalist)
    return textResult


def cesarToText(text, key, alphalist):
    textEncrypted = ""
    for char in text:
        textEncrypted = textEncrypted + cesarToChar(char, key, alphalist)
    return textEncrypted


def cesarTofile(filename, key, alphalist):
    textEncrypted = openFile(filename + "_code.txt")
    textresult = cesarToText(textEncrypted, key, alphalist)
    writeFile(filename + "_decode.txt", textresult)


def fileToCesar(filename, key, alphalist):
    text = openFile(filename + ".txt")
    cleanText = clean(text)
    textEncrypted = textToCesar(cleanText, key, alphalist)
    writeFile(filename + "_code.txt", textEncrypted)


def textToVig(text, key, alphalist):
    text_result = ""
    i = 0
    for char in text:
        text_result = text_result + charToCesar(char, key[i], alphalist)
        i += 1
        if i >= len(key):
            i = 0
    return text_result


def vigToText(text, key, alphalist):
    text_result = ""
    i = 0
    for char in text:
        text_result = text_result + cesarToChar(char, key[i], alphalist)
        i += 1
        if i >= len(key):
            i = 0
    return text_result


def fileToVig(filename, key, alphalist):
    text = openFile(filename + ".txt")
    cleanText = clean(text)
    textEncrypted = textToVig(cleanText, key, alphalist)
    writeFile(filename + "_code.txt", textEncrypted)


def vigToFile(filename, key, alphalist):
    textEncrypted = openFile(filename + "_code.txt")
    textresult = vigToText(textEncrypted, key, alphalist)
    writeFile(filename + "_decode.txt", textresult)


def attaque_brute_force_sa(text, alphalist):
    bool_input = True
    alphalist2 = list(alphalist)
    i = 1
    while (bool_input):
        result = cesarToText(text, i, alphalist2)
        print(result)
        inputuser = input("\n  Pour tester la clé suivante, appuyer sur la touche N, sinon S pour stopper.")
        if inputuser == "S":
            bool_input = False
            break;
        i += 1


def e_attack(text, alphalist):
    print(text)
    bool_input = True
    alphalist2 = list(alphalist)
    occurenceFr = ["e", "a", "i", "s"]
    nbrTest = 0
    occurenceOfLetterList = occurenceOfLetter(text, alphalist2)
    rateOfLetterList = list(rateOfLetter(text, occurenceOfLetterList))
    wordDict = dict.fromkeys(alphalist2)
    for letter, i in zip(alphalist2, range(0, len(alphalist2))):
        wordDict[letter] = rateOfLetterList[i]
    wordDictSorted = dict(sorted(wordDict.items(), key=lambda item: item[1], reverse=True))
    while (bool_input):
        print("lettre avec la " + str(nbrTest + 1) + " occurence est " + list(wordDictSorted.keys())[nbrTest])
        for occurence in occurenceFr:
            key = abs(alphalist.index(occurence) - alphalist.index(list(wordDictSorted.keys())[nbrTest]))
            result = cesarToText(text, key, alphalist2)
            print("test sur la repetition de la lettre " + occurence)
            print("la clef est :" + str(key))
            print(result)
            inputuser = input("\n  Pour tester la clé suivante, appuyer sur la touche N, sinon S pour stopper.")
            if inputuser == "S":
                bool_input = False
                return result
        nbrTest += 1

def e_attack_decode(text, alphalist):
    alphalist2 = list(alphalist)
    nbrTest = 0
    occurenceOfLetterList = occurenceOfLetter(text, alphalist2)
    rateOfLetterList = list(rateOfLetter(text, occurenceOfLetterList))
    wordDict = dict.fromkeys(alphalist2)
    for letter, i in zip(alphalist2, range(0, len(alphalist2))):
        wordDict[letter] = rateOfLetterList[i]
    wordDictSorted = dict(sorted(wordDict.items(), key=lambda item: item[1], reverse=True))
    key = abs(alphalist.index("e") - alphalist.index(list(wordDictSorted.keys())[nbrTest]))
    return key

def indexC(text, alphalist):
    text = clean(text).replace(" ", "")
    indexC = 0
    lenText = len(text)
    for occurenceLetter in occurenceOfLetter(text, alphalist):
        indexC += ((occurenceLetter / lenText) * ((occurenceLetter - 1) / (lenText - 1)))
    return indexC

def key_len(text , len_word):
    dico_occurence =  {}
    for i in range(0, len(text) - 2):
        world = text[i:i + len_word]
        if world in dico_occurence.keys():
            dico_occurence[world].append(i)
        else: dico_occurence[world] = [i]
    distance = []
    for vals in dico_occurence.values():
        if len(vals) > 1:
            for i in range(0, len(vals) - 1):
                distance.append(abs(vals[i] + len_word - vals[i+1]) + len_word)
    return distance

def atk_double(text, alphalist):
    split_text = text.split(" ")
    potential_key = []
    for word in split_text:
        for char in word[-3:]:
            if word.count(char) > 1:
                print(char)
                key = abs(alphalist.index(char) - 26)
                if key not in potential_key:
                    potential_key.append(key)
    for key in potential_key:
        text_result = cesarToText(text, key, alphalist)
        print(text_result + " || la Key est " + str(key))

    #pas possible pour vigenere car on ne verra pas les lettres en double

def decode(filename):
    text = clean(openFile(filename))
    d_all = []
    split = 2
    while split != len(text):
        split += 1
        d = key_len(text, split)
        d_all.extend(d)
        if split == 10:
            break
    len_key = math.gcd(*d_all)
    l = []
    i = 0
    for char in text:
        if len(l) != i:
            l[i] = l[i] + char
        else:
            l.append(char)
        i+= 1
        if len_key == i:
            i = 0
    e_atk = []
    for split_text in l:
        e_atk.append(e_attack_decode(split_text, alphalist))
    result = vigToText(text,e_atk,alphalist)
    print(result)

if __name__ == '__main__':
    alphalist = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                 "u", "v", "w", "x", "y", "z"]
    text = "erqmrxu d wrxwhv hw d wrxv vrbhc ohv elhqyhqxhv"

    #a = attaque_brute_force_sa(text, alphalist)
    #print(a)

    #b = e_attack(text, alphalist)
    #print(b)

    #decode("test.txt")

    text_double = "hokt rk hutpuax nussk kz vxatkrrk"

    atk_double(text_double,alphalist)




